<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tabfmt {
                    border: 1px ;
                    }
                    td.colfmt {
                    border: 1px ;
                    background-color: red;
                    color: white;
                    text-align:right;
                    }
                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }
                </style>
            </head>

            <body>
                <table class="tabfmt">
                    <tr>
                        <th style="width:250px">Name</th>
                        <th style="width:350px">Energy</th>
                        <th style="width:250px">Type</th>
                        <th style="width:250px">Protein</th>
                        <th style="width:250px">Fat</th>
                        <th style="width:250px">Carbohydrate</th>
                        <th style="width:250px">Production</th>
                        <th style="width:250px">Address</th>
                        <th style="width:250px">Number</th>
                        <th style="width:250px">Weight</th>
                    </tr>
                    <xsl:for-each select="Candies/Candie">
                        <xsl:sort select="Name"/>
                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="Name" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Energy" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Type" />
                            </td>
<!--                            <td class="colfmt">-->
<!--                                <xsl:for-each select="Ingredients">-->
<!--                                    <xsl:value-of select="current()" />-->
<!--                                </xsl:for-each>-->
<!--                            </td>-->
                            <td class="colfmt">
                                <xsl:value-of select="Nutrient/Protein" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Nutrient/Fat" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Nutrient/Carbohydrate" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Production/ProductionName" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Production/Address" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Production/Number" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="Weight" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>