package com.yasiuraroman.view;

import com.yasiuraroman.controller.XMLTaskControllerImpl;
import com.yasiuraroman.controller.XMLTaskController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.*;

public class View {
    private Logger logger = LogManager.getLogger(View.class.getName());
    private Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private XMLTaskController controller;
    private ResourceBundle menuBundle = ResourceBundle.getBundle("Menu");

    public View() {
        controller = new XMLTaskControllerImpl();
        createMenu();
        initMenuCommands();
    }

    private void createMenu(){
        menu = new LinkedHashMap<>();
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.addAll(menuBundle.keySet());
        for (String key : sortedSet
        ) {
            menu.put(key,menuBundle.getString(key));
        }
    }

    //-------------------------------------------------------------------------
    private void initMenuCommands() {
        methodsMenu = new HashMap<>();
        methodsMenu.put("1", this::showDOMParserExample);
        methodsMenu.put("2", this::showSAXParserExample);
        methodsMenu.put("3", this::showSTAXParserExample);
        methodsMenu.put("4", this::showXMLToHTMLExample);
        methodsMenu.put("5", this::showChangeRootExample);
    }

    private void showDOMParserExample() {
        try {
            controller.domParser().stream().forEach((e) -> logger.info(e));
        } catch (SAXException | ParserConfigurationException | IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void showSAXParserExample() {
        try {
            controller.saxParser().stream().forEach((e) -> logger.info(e));
        } catch (SAXException | ParserConfigurationException | IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void showSTAXParserExample() {
        try {
            controller.staxParser().stream().forEach((e) -> logger.info(e));
        } catch (IOException | ParserConfigurationException | XMLStreamException | SAXException e) {
            logger.error(e.getMessage());
        }
    }

    private void showXMLToHTMLExample() {
        controller.toHTML();
    }

    private void showChangeRootExample() {
        try {
            controller.changeRootElement();
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            logger.error(e.getMessage());
        }
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
