package com.yasiuraroman.controller;

import com.yasiuraroman.model.Candie;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.*;
import java.io.IOException;
import java.util.List;

public interface XMLTaskController {

    void changeRootElement() throws ParserConfigurationException, IOException, SAXException, TransformerException;

    void toHTML();

    List<Candie> domParser() throws SAXException, ParserConfigurationException, IOException;

    List<Candie> staxParser() throws SAXException, ParserConfigurationException, IOException, XMLStreamException;

    List<Candie> saxParser() throws SAXException, ParserConfigurationException, IOException;
}
