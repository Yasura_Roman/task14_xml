package com.yasiuraroman.controller;

import com.yasiuraroman.model.Candie;
import com.yasiuraroman.parser.CandiesDomParser;
import com.yasiuraroman.parser.CandiesSAXParser;
import com.yasiuraroman.parser.CandiesSTAXParser;
import com.yasiuraroman.util.XMLConverterToHTML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class XMLTaskControllerImpl implements XMLTaskController {
    public void changeRootElement()
            throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(".\\src\\main\\resources\\myCandiesXML.xml"));
        Element element = document.getDocumentElement();
        Element element1 = document.createElement("newRoot");
        while (element.hasChildNodes()) {
            element1.appendChild(element.getFirstChild());
        }
        element.getParentNode().replaceChild(element1, element);
        TransformerFactory tranFactory = TransformerFactory.newInstance();
        Transformer aTransformer = tranFactory.newTransformer();
        Source src = new DOMSource(document);
        Result dest = new StreamResult(new File(".\\src\\main\\resources\\myCandiesXMLWithNewRoot.xml"));
        aTransformer.transform(src, dest);
    }

    public void toHTML(){
        Source xml = new StreamSource(new File(".\\src\\main\\resources\\myCandiesXML.xml"));
        Source xslt = new StreamSource(new File(".\\src\\main\\resources\\Candieshtml.xsl"));
        XMLConverterToHTML.convertXMLToHTML(xml, xslt);
    }


    public List<Candie> domParser() throws SAXException, ParserConfigurationException, IOException {
        CandiesDomParser parser = new CandiesDomParser();
        return parser.parseDocument(".\\src\\main\\resources\\myCandiesXML.xml");
    }

    public List<Candie> staxParser() throws SAXException, ParserConfigurationException, IOException, XMLStreamException {
        CandiesSTAXParser parser = new CandiesSTAXParser();
        return parser.parseDocument(".\\src\\main\\resources\\myCandiesXML.xml");
    }

    public List<Candie> saxParser() throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        Schema schema = factory.newSchema(new File(".\\src\\main\\resources\\CandiesShema.xsd"));
        saxParserFactory.setSchema(schema);

        SAXParser saxParser = saxParserFactory.newSAXParser();
        CandiesSAXParser saxHandler = new CandiesSAXParser();
        saxParser.parse(new File(".\\src\\main\\resources\\myCandiesXML.xml"), saxHandler);
        return saxHandler.getCandies();
    }
}
