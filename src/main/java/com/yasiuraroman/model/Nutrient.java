package com.yasiuraroman.model;

public class Nutrient {
    private int protein;
    private int fat;
    private int carbohydrate;

    public Nutrient() {
    }

    public Nutrient(int protein, int fat, int carbohydrate) {
        this.protein = protein;
        this.fat = fat;
        this.carbohydrate = carbohydrate;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getFat() {
        return fat;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public int getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(int carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    @Override
    public String toString() {
        return "Nutrient [" +
                "protein" + protein +
                ", fat" + fat +
                ", carbohydrate" + carbohydrate +
                "]";
    }
}
