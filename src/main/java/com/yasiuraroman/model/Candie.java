package com.yasiuraroman.model;

import java.util.Comparator;
import java.util.List;

public class Candie {
    private String name;
    private Integer energy;
    private CandieType type;
    private List<Ingredient> ingredients;
    private Nutrient values;
    private Production production;
    private int weight;

    public Candie() {
    }

    public Candie(String name, Integer energy, CandieType type, List<Ingredient> ingredients, Nutrient values, Production production, int weight) {
        this.name = name;
        this.energy = energy;
        this.type = type;
        this.ingredients = ingredients;
        this.values = values;
        this.production = production;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public CandieType getType() {
        return type;
    }

    public void setType(CandieType type) {
        this.type = type;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Nutrient getValues() {
        return values;
    }

    public void setValues(Nutrient values) {
        this.values = values;
    }

    public Production getProduction() {
        return production;
    }

    public void setProduction(Production production) {
        this.production = production;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Candie[" +
                "name = " + name +
                ", energy = " + energy +
                ", type = " + type +
                ", ingredients = " + ingredients +
                ", values = " + values +
                ", production = " + production +
                ", weight = " + weight +
                "]";
    }

    public Comparator<Candie> getNameComparator() {
        return Comparator.comparing(Candie::getName);
    }
}
