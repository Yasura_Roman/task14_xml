package com.yasiuraroman.parser;

import com.yasiuraroman.model.*;
import com.yasiuraroman.util.VoidExecutor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class CandiesDomParser {
    private List<Candie> candies = new ArrayList<>();;
    private Candie candie = new Candie();
    private Map<String,Boolean> currentElement;
    private Map<String, VoidExecutor> executeElement;

    public CandiesDomParser() {
        currentElement = new TreeMap<>();
        currentElement.put("Name", false);
        currentElement.put("Energy", false);
        currentElement.put("Type", false);
        currentElement.put("Ingredients", false);
        currentElement.put("Ingredient", false);
        currentElement.put("IngredientName", false);
        currentElement.put("Value", false);
        currentElement.put("Nutrient", false);
        currentElement.put("Protein", false);
        currentElement.put("Fat", false);
        currentElement.put("Carbohydrate", false);
        currentElement.put("Production", false);
        currentElement.put("ProductionName", false);
        currentElement.put("Address", false);
        currentElement.put("Number", false);
        currentElement.put("Weight", false);

        executeElement = new HashMap<>();
        executeElement.put("Name", this::setCandieName);
        executeElement.put("Energy", this::setCandieEnergy);
        executeElement.put("Type", this::setCandieType);
        executeElement.put("Ingredients", (s) -> {});
        executeElement.put("Ingredient", (s) -> {});
        executeElement.put("IngredientName", this::setIngredientName);
        executeElement.put("Value", this::setIngredientValue);
        executeElement.put("Nutrient", this::setNutrient);
        executeElement.put("Protein", this::setNutrientProtein);
        executeElement.put("Fat", this::setNutrientFat);
        executeElement.put("Carbohydrate", this::setNutrientCarbohydrate);
        executeElement.put("Production", this::setProduction);
        executeElement.put("ProductionName", this::setProductionName);
        executeElement.put("Address", this::setProductionAddress);
        executeElement.put("Number", this::setProductionNumber);
        executeElement.put("Weight", this::setWeight);
    }


//-----------------------------------------------------------------------------------------------------------------

    private void addCandieToList( String st) {
        this.candie = new Candie();
        candies.add(candie);
    }

    private void setCandieEnergy(String s) {
        candie.setEnergy(Integer.parseInt(s));
    }

    private void setCandieName(String s) {
        this.candie = new Candie();
        candies.add(candie);
        candie.setName(s);
    }
    private void setIngredientValue(String s) {
        candie.getIngredients().get(candie.getIngredients().size() - 1).setValue(Integer.parseInt(s));
    }

    private void setIngredientName(String s) {
        if (candie.getIngredients() == null){
            candie.setIngredients(new LinkedList<>());
        }
        candie.getIngredients().add(new Ingredient());
        candie.getIngredients().get(candie.getIngredients().size() - 1).setName(s);
    }

    private void setCandieType(String s) {
        for (CandieType type : CandieType.values()
        ) {
            if(type.toString().equalsIgnoreCase(s)){
                candie.setType(type);
            }
        }
    }

    private void setNutrient(String s) {
        candie.setValues(new Nutrient());
    }

    private void setNutrientProtein(String s) {
        candie.getValues().setProtein(Integer.parseInt(s));
    }

    private void setNutrientFat(String s) {
        candie.getValues().setFat(Integer.parseInt(s));
    }

    private void setNutrientCarbohydrate(String s) {
        candie.getValues().setCarbohydrate(Integer.parseInt(s));
    }

    private void setProduction(String s) {
        candie.setProduction(new Production());
    }

    private void setProductionName(String s) {
        candie.getProduction().setName(s);
    }

    private void setProductionAddress(String s) {
        candie.getProduction().setAddress(s);
    }

    private void setProductionNumber(String s) {
        candie.getProduction().setNumber(s);
    }

    private void setWeight(String s) {
        candie.setWeight(Integer.parseInt(s));
    }

    //==============================================================================

    public List<Candie> parseDocument(String path)
            throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(path));
        document.getDocumentElement().normalize();
        NodeList candieNodes = document.getDocumentElement().getElementsByTagName("Candie");
        for (int i = 0; i < candieNodes.getLength(); i++) {
            Node candieNod = candieNodes.item(i);
            NodeList childNodes = candieNod.getChildNodes();
            for (int j = 0; j < childNodes.getLength(); j++) {
                Node element = childNodes.item(j);
                if (element.getNodeType() == Node.ELEMENT_NODE) {
                    String elementName = ((Element) element).getTagName();
                    executeElement.get(elementName).execute(element.getTextContent());
                }
                if (element.hasChildNodes()) {
                    NodeList child = element.getChildNodes();
                    for (int k = 0; k < child.getLength(); k++) {
                        Node childElement = child.item(k);
                        if (childElement.getNodeType() == Node.ELEMENT_NODE) {
                            String elementName = ((Element) childElement).getTagName();
                            executeElement.get(elementName).execute(childElement.getTextContent());
                        }
                    }
                }
            }
        }
        return candies;
    }
}
