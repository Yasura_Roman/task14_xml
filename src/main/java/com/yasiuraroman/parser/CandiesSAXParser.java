package com.yasiuraroman.parser;

import com.yasiuraroman.model.*;
import com.yasiuraroman.util.VoidExecutor;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

public class CandiesSAXParser extends DefaultHandler {
    private List<Candie> candies = new ArrayList<>();;
    private Candie candie = null;
    private Map<String,Boolean> currentElement;
    private Map<String, VoidExecutor> executeElement;

    public CandiesSAXParser() {
        currentElement = new TreeMap<>();
        currentElement.put("Name", false);
        currentElement.put("Energy", false);
        currentElement.put("type", false);
        currentElement.put("IngredientName", false);
        currentElement.put("Value", false);
        currentElement.put("Nutrient", false);
        currentElement.put("Protein", false);
        currentElement.put("Fat", false);
        currentElement.put("Carbohydrate", false);
        currentElement.put("Production", false);
        currentElement.put("ProductionName", false);
        currentElement.put("Address", false);
        currentElement.put("Number", false);
        currentElement.put("Weight", false);

        executeElement = new HashMap<>();
        executeElement.put("Name", this::setCandieName);
        executeElement.put("Energy", this::setCandieEnergy);
        executeElement.put("type", this::setCandieType);
        executeElement.put("IngredientName", this::setIngredientName);
        executeElement.put("Value", this::setIngredientValue);
        executeElement.put("Nutrient", this::setNutrient);
        executeElement.put("Protein", this::setNutrientProtein);
        executeElement.put("Fat", this::setNutrientFat);
        executeElement.put("Carbohydrate", this::setNutrientCarbohydrate);
        executeElement.put("Production", this::setProduction);
        executeElement.put("ProductionName", this::setProductionName);
        executeElement.put("Address", this::setProductionAddress);
        executeElement.put("Number", this::setProductionNumber);
        executeElement.put("Weight", this::setWeight);
    }


//-----------------------------------------------------------------------------------------------------------------

    private void addCandieToList( String st) {
        this.candie = new Candie();
        candies.add(candie);
    }

    private void setCandieEnergy(String s) {
        candie.setEnergy(Integer.parseInt(s));
    }

    private void setCandieName(String s) {
        this.candie = new Candie();
        candies.add(candie);
        candie.setName(s);
    }
    private void setIngredientValue(String s) {
        candie.getIngredients().get(candie.getIngredients().size() - 1).setValue(Integer.parseInt(s));
    }

    private void setIngredientName(String s) {
        if (candie.getIngredients() == null){
            candie.setIngredients(new LinkedList<>());
        }
        candie.getIngredients().add(new Ingredient());
        candie.getIngredients().get(candie.getIngredients().size() - 1).setName(s);
    }

    private void setCandieType(String s) {
        for (CandieType type : CandieType.values()
             ) {
            if(type.toString().equalsIgnoreCase(s)){
                candie.setType(type);
            }
        }
    }

    private void setNutrient(String s) {
        candie.setValues(new Nutrient());
    }

    private void setNutrientProtein(String s) {
        candie.getValues().setProtein(Integer.parseInt(s));
    }

    private void setNutrientFat(String s) {
        candie.getValues().setFat(Integer.parseInt(s));
    }

    private void setNutrientCarbohydrate(String s) {
        candie.getValues().setCarbohydrate(Integer.parseInt(s));
    }

    private void setProduction(String s) {
        candie.setProduction(new Production());
    }

    private void setProductionName(String s) {
        candie.getProduction().setName(s);
    }

    private void setProductionAddress(String s) {
        candie.getProduction().setAddress(s);
    }

    private void setProductionNumber(String s) {
        candie.getProduction().setNumber(s);
    }

    private void setWeight(String s) {
        candie.setWeight(Integer.parseInt(s));
    }

//-----------------------------------------------------------------------------------------------------------------

    public List<Candie> getCandies() {
        return candies;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        Optional<Map.Entry<String, Boolean>> optional = currentElement.entrySet().stream()
                .filter(e -> e.getKey().equalsIgnoreCase(qName))
                .findFirst();
        if (optional.isPresent()) {
            optional.get().setValue(true);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currentElement.entrySet().forEach(e -> e.setValue(false));
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        Optional<Map.Entry<String, Boolean>> activeElement =  currentElement.entrySet().stream()
                .filter(e -> e.getValue().booleanValue())
                .findFirst();
        if (activeElement.isPresent()) {
            executeElement.get(activeElement.get().getKey()).execute(new String(ch, start, length).toLowerCase());
        }
    }
}