package com.yasiuraroman.util;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class XMLConverterToHTML {
    public static void convertXMLToHTML(Source xml, Source xslt) {
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter(".\\src\\main\\resources\\myCandiesXML.html");
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(xslt);
            transformer.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
        } catch (IOException | TransformerFactoryConfigurationError | TransformerException e) {
            e.printStackTrace();
        }
    }
}
