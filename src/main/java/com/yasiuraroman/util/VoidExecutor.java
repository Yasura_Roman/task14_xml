package com.yasiuraroman.util;
@FunctionalInterface
public interface VoidExecutor {
    void execute(String st);
}
