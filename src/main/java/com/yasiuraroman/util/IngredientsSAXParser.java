package com.yasiuraroman.util;

import com.yasiuraroman.model.Ingredient;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class IngredientsSAXParser extends DefaultHandler {
    private List<Ingredient> Ingredients = null;
    private Ingredient ingredient = null;
    private Map<String,Boolean> currentElement;
    private Map<String, VoidExecutor> executeElement;

    public IngredientsSAXParser() {
        currentElement = new HashMap<>();
        currentElement.put("Ingredient", false);
        currentElement.put("Name", false);
        currentElement.put("Value", false);

        executeElement = new HashMap<>();
        executeElement.put("Ingredient", this::addIngredientToList);
        executeElement.put("Name", this::setIngredientName);
        executeElement.put("Value", this::setIngredientValue);
    }

    private void addIngredientToList(String s) {
        ingredient = new Ingredient();
    }

    private void setIngredientName(String s) {
        ingredient.setName(s);
    }

    private void setIngredientValue(String s) {
        ingredient.setValue(Integer.parseInt(s));
    }

    public List<Ingredient> getIngredients() {
        return Ingredients;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        Optional<Map.Entry<String, Boolean>> optional = currentElement.entrySet().stream()
                .filter(e -> e.getKey().equalsIgnoreCase(qName))
                .findFirst();
        if (optional.isPresent()) {
            optional.get().setValue(true);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currentElement.entrySet().forEach(e -> e.setValue(false));
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        Optional<Map.Entry<String, Boolean>> activeElement =  currentElement.entrySet().stream()
                .filter(e -> e.getValue().booleanValue())
                .findFirst();
        if (activeElement.isPresent()) {
            executeElement.get(activeElement.get().getKey()).execute(new String(ch, start, length).toLowerCase());
        }
    }
}
